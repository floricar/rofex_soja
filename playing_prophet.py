#%%
# %matplotlib inline
import pandas as pd
from fbprophet import Prophet

#%%
import matplotlib.pyplot as plt
plt.style.use('fivethirtyeight')

#%%
df = pd.read_csv('prophet-example/AirPassengers.csv')
df_ = df.copy()
df.head(5)

#%%
df.dtypes

#%%
df['Month'] = pd.DatetimeIndex(df['Month'])
df.dtypes

#%%
df = df.rename(columns={'Month': 'ds', 'AirPassengers': 'y'})
df.head(5)

#%%
ax = df_.plot(figsize=(12, 8))
ax.set_ylabel('Monthly Number of Airline Passengers')
ax.set_xlabel('Date')
plt.show()

#%%
my_model = Prophet(interval_width=0.95)

#%%
my_model.fit(df)

#%%
future_dates = my_model.make_future_dataframe(periods=36, freq='MS')
future_dates.tail()

#%%
forecast = my_model.predict(future_dates)
forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail()

#%%
my_model.plot(forecast, uncertainty=True)


#%%
my_model.plot_components(forecast)



#%%
